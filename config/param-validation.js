const Joi = require('joi');

const phoneRegEx = /[2-9]{1}\d{2}/;

module.exports = {
  // POST /api/users
  createUser: {
    body: {
      /* username: Joi.string().required(),
      mobileNumber: Joi.number().required() */
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  }
};
