/**
 * @type {{type: StringConstructor, formType: string}}
 */
const InputText = {
  type: String,
  formType: 'text',
};

/**
 * @type {{type: NumberConstructor, formType: string}}
 */
const InputNumber = {
  type: Number,
  formType: 'number',
};

/**
 * @type {{type: BooleanConstructor, formType: string}}
 */
const InputCheckbox = {
  type: Boolean,
  formType: 'checkbox',
};

/**
 * Form Types Module
 * ---------------------
 * Define fields types regarding data grids, forms ...
 */
module.exports = {
  InputCheckbox,
  InputNumber,
  InputText,
};
