const { User, UserSchemaPublicProps } = require('./user.model');

/**
 * Load user and append to req.
 */
const load = (req, res, next, id) => {
  User.get(id)
    .then((user) => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
};

/**
 * Get user
 */
const get = (req, res) => res.json(req.user);

/**
 * Create new user
 */
const create = (req, res) => {
  const user = new User({
    username: req.body.username,
    mobileNumber: req.body.mobileNumber,
  });

  user.save()
    .then(
      savedUser => res.json(savedUser),
      error => res.status(error.statusCode || 500).json({ error })
    );
};

/**
 * Update existing user
 */
const update = (req, res) => {
  const { user } = req;

  // Get user public props from request body
  Object.keys(UserSchemaPublicProps).forEach(
    (publicProp) => {
      user[publicProp] = req.body[publicProp];
    }
  );

  // Validate previuslly assigned properties, with shema validation
  user.validate().then(
    () => user.save().then(
        result => res.json(result),
        error => res.status(500).json({ error })
    ),
    (validationError) => {
      const { errors } = validationError;

      return res.status(500).json({ errors });
    }
  );
};

/**
 * Get user list.
 */
const list = (req, res, next) => {
  const { limit = 50, skip = 0 } = req.query;

  User.list({ limit, skip })
    .then(users => res.json(users))
    .catch(e => next(e));
};

/**
 * Delete user.
 */
const remove = (req, res, next) => {
  const user = req.user;

  user.remove()
    .then(deletedUser => res.json(deletedUser))
    .catch(e => next(e));
};

/**
 * Return User Schema
 */
const schema = (req, res) => res.json(UserSchemaPublicProps);

module.exports = { load, get, create, update, list, remove, schema };
