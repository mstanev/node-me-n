const express = require('express');
const userCtrl = require('./user.controller');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(userCtrl.list)
  .post(userCtrl.create);

router.route('/schema')
  .get(userCtrl.schema);

router.route('/:userId')
  .get(userCtrl.get)
  .put(userCtrl.update)
  .delete(userCtrl.remove);

router.param('userId', userCtrl.load);

module.exports = router;
