const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const formTypes = require('../helpers/formTypes');

// TODO: try to make formType schema .etc
/**
 * @type {{username: {required: *[], type: StringConstructor, formType: string}, mobileNumber: {minlength: *[], maxlength: *[], type: NumberConstructor, formType: string}, OtherNewProp: {default: boolean, type: BooleanConstructor, formType: string}, LastBoolean: {default: boolean, required: *[], type: BooleanConstructor, formType: string}}}
 */
const UserSchemaPublicProps = {
  username: {
    required: [true, 'Field is Required'],
    ...formTypes.InputText,
  },
  mobileNumber: {
    minlength: [6, 'Too Short, Minimum 6'],
    maxlength: [12, 'Too Long , Maximum 12'],
    ...formTypes.InputNumber,
  },
  OtherNewProp: {
    default: false,
    ...formTypes.InputCheckbox,
  },
  LastBoolean: {
    default: true,
    required: [true, 'Field is Required'],
    ...formTypes.InputCheckbox,
  },
  selectItem: {
    type: Number,
    default: 1,
    formType: 'select',
    options: [
      {val: 1, text: '--'},
      {val: 2, text: 'option'},
      {val: 3, text: 'third'}
    ],
  },
};

const UserSchema = new mongoose.Schema({
  ...UserSchemaPublicProps,
  createdAt: {
    type: Date,
    default: Date.now
  }
});

UserSchema.statics = {
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};


const User = mongoose.model('User', UserSchema);

module.exports = {
  User, UserSchemaPublicProps
};
